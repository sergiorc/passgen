#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;


#define MAX_PASSWORD_LENGTH 14


/*
* Function prototypes
*/
int loadPasswordPatterns(const string fileName, vector<string> &patterns);

void onlyOneUppercaseCharacter(string word, vector<string> &patterns);
void allCharatersToUpperCase(string workd, vector<string> &patterns);
void numbersAtTheEnd(const string textPattern, vector<string> &patterns);
void numbersAtTheBeginning(const string word, vector<string> &patterns);
void underscoreSeparated(const string word, vector<string> &patterns);
void replaceVowelsByNumbers(const string word, vector<string> &patterns);
void consonantsToUppercase(const string word, vector<string> &patterns);
void allCombinationsFromChars(const string word, vector<string> &patterns);

bool isConsonant(char c);

int main()
{
    vector<string> patterns;
    vector<string> generatedPatterns;

    int result = loadPasswordPatterns("patterns.txt", patterns);

    if (result == -1)
    {
        cout << "*** File containing patterns does not exists\n";
        return 0;
    }
    if (patterns.size() <= 0)
    {
        cout << "No patterns loaded from file\n";
        return 0;
    }

    cout << patterns.size() << " patterns loaded\n"; 

    // Loop through all text patterns loaded from file
    for(string text : patterns)
    {
        onlyOneUppercaseCharacter(text, generatedPatterns);
        allCharatersToUpperCase(text, generatedPatterns);
        numbersAtTheEnd(text, generatedPatterns);
        numbersAtTheBeginning(text, generatedPatterns);
        underscoreSeparated(text, generatedPatterns);
        //replaceVowelsByNumbers(text, generatedPatterns);
        //consonantsToUppercase(text, generatedPatterns);
    }

    cout << "Saving passwords...\n";
    ofstream passwordsFile("passwords.txt");

    if (!passwordsFile)
    {
        cerr << "Error: Could not create passwords.txt" << "\n";
        return 0;
    }
    
    for(string pattern: generatedPatterns)
    {
        passwordsFile << pattern + '\n';        
    }

    passwordsFile.close();
    cout << generatedPatterns.size() + " passwords saved\n";    

    return 0;
}

void onlyOneUppercaseCharacter(string word, vector<string> &patterns)
{
    for(int i=0; i < word.length(); i++)
    {
        if (!isalpha(word[i])) 
            continue;

        if (i > 0 && i < sizeof(word))
            word[i-1] = (char)tolower(word[i-1]);

        // and set the current one to uppercase
        word[i] = (char)toupper(word[i]);

        patterns.push_back(word);
    }
}

void allCharatersToUpperCase(string word, vector<string> &patterns)
{
    for(int i=0; i < word.length(); i++)
    {
        if (!isalpha(word[i]))
        {
            cerr << word[i] << " is not valid" << "\n";
            continue;
        }

        word[i] = (char)toupper(word[i]);
    }

    patterns.push_back(word);
}

void numbersAtTheEnd(const string word, vector<string> &patterns) 
{
    string numeric;
    string password;

    for(int i=1; i < 11; i++)
    {
        numeric += to_string(i);
        password = word + numeric;

        if (password.length() > MAX_PASSWORD_LENGTH)
        {
            int limit = password.length() - MAX_PASSWORD_LENGTH;
            numeric = numeric.substr(0, limit);

            password = word + numeric;
        }

        patterns.push_back(password);
        password = "";
    }   
}

void underscoreSeparated(const string word, vector<string> &patterns)
{
    int numberOfPasswords = 900;    
    int max = 999999;
    int min = 1;
    int i;

    string password;

    /*
    * Random numeric termination
    */
    for(i=0; i < numberOfPasswords; i++)
    {
        int randomNumber = rand() % (max - min + 1) + min;
        // Concatenate it to the password separated by 
        // underscore
        password = word + "_" + to_string(randomNumber);

        if (password.length() > MAX_PASSWORD_LENGTH)
            password = password.substr(0, MAX_PASSWORD_LENGTH - 1);

        patterns.push_back(password);         
    }

    /*
    *   Incremental numeric termination
    */
    password = "";
    string numericTermination = "";

    for(i=1; i < 11; i++)
    {
        numericTermination += to_string(i);
        password = word + "_" + numericTermination;

        if (password.length() > MAX_PASSWORD_LENGTH)
            password = password.substr(0, MAX_PASSWORD_LENGTH - 1);        

        patterns.push_back(password);
    }    
}

void numbersAtTheBeginning(const string word, vector<string> &patterns)
{
    int numericStringsCount = 11;

    string password;
    string numeric;

    for(int i=1; i < numericStringsCount; i++)
    {
        numeric += to_string(i);
        password = numeric + word;

        int totalPasswordLength = numeric.length() + word.length();

        if (totalPasswordLength > MAX_PASSWORD_LENGTH)
        {
            // Detach the last N characters from the numeric representation
            int charactersToDetach = totalPasswordLength - MAX_PASSWORD_LENGTH;
            numeric = numeric.substr(0, charactersToDetach);
            password = numeric + word;
        }

        patterns.push_back(password);
    }   
}

void replaceVowelsByNumbers(const string word, vector<string> &patterns)
{
    /*
    a,e,i,o.u
    0 -> o,O
    1 -> i,I
    2 -> to, two
    3 -> e,E
    4 -> for,a,A
    5 -> s,S
    6 -> g,G
    7 -> t,T
    8 ->
    9 ->
    */

    string password = word;

    for (int i=0; i < word.length(); i++)
    {
        if ((char)tolower(word[i]) == 'a') password[i] = '4';
        if ((char)tolower(word[i]) == 'e') password[i] = '3';        
        if ((char)tolower(word[i]) == 'i') password[i] = '1';
        if ((char)tolower(word[i]) == 'o') password[i] = '0';
        //if ((char)tolower(word[i]) == 'u') password[i] = 'v';
    }

    patterns.push_back(password);
}

void consonantsToUppercase(const string word, vector<string> &patterns)
{
    string password = word;

    for(int i=0; i < word.length(); i++) {
        if (isConsonant((char)word[i]))
            password[i] = toupper((char)word[i]);
    }

    patterns.push_back(password);
}

void allCombinationsFromChars(const string word, vector<string> &patterns)
{
    string password;
    string currentChar;

    char letters[] = {'a','b','c','d'};    

    for(int i=0; i < 4; i++)
    {
        char letter = l[i];
        
    }

}

bool isConsonant(char c)
{
    char vowels[10] = {'a','e','i','o','u','A','E','I','O','U'};
    char character = tolower(c);

    for(int i=0; i < 10; i++)
    {
        if (character == vowels[i])
            return false;
    }

    return true;
}

int loadPasswordPatterns(const string fileName, vector<string> &patterns)
{
    ifstream inputFile(fileName);

    if (!inputFile)
        return -1;

    /*
    * I could use "for loop" for reading file contents
    * for(string line; getline(inputFile, line);) {}
    */
    string line;

    while(getline(inputFile, line))
    {
        string tmp = line;
        tmp.erase(remove(tmp.begin(), tmp.end(), '\r'), tmp.end());        
        tmp.erase(remove(tmp.begin(), tmp.end(), '\n'), tmp.end());        
        patterns.push_back(tmp);
    }

    inputFile.close();
}
